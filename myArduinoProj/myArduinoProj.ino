int IN1=7;
int IN2=6;
int ENA=5;
int i;
int k;

byte sensorPin = 3;


#define echoPin 9 // Echo Pin
#define trigPin 8 // Trigger Pin
#define LEDPin 13

int maximumRange = 200; // Maximum range needed
int minimumRange = 0; // Minimum range needed
long duration, distance; // Duration used to calculate distance

#include <Adafruit_SSD1306.h>
Adafruit_SSD1306 display(4);
String incomingByte;
boolean n=false;

void setup()
{
 for (int i = 5; i <8; i ++)
   {
     pinMode(i, OUTPUT);  
   }
   Serial.begin(115200);


   display.begin(SSD1306_SWITCHCAPVCC, 0x3c);
display.clearDisplay();
display.setTextSize(3.5);
display.setTextColor(WHITE);
display.setCursor(0,0);
display.println("BLANK..");
display.display();
delay(2000);


pinMode(sensorPin,INPUT);
 // pinMode(indicator,OUTPUT);
 


 Serial.begin (9600);
 pinMode(trigPin, OUTPUT);
 pinMode(echoPin, INPUT);
 pinMode(LEDPin, OUTPUT); // Use LED indicator (if required)


}
void loop()
{





digitalWrite(trigPin, LOW);
 delayMicroseconds(2);

 digitalWrite(trigPin, HIGH);
 delayMicroseconds(10);
 digitalWrite(trigPin, LOW);

 duration = pulseIn(echoPin, HIGH);
 
 //Calculate the distance (in cm) based on the speed of sound.
 distance = duration/58.2;
 
 if (distance >= maximumRange || distance <= minimumRange){
 /* Send a negative number to computer and Turn LED ON
 to indicate "out of range" */
 Serial.println("-1");
 digitalWrite(LEDPin, HIGH);
 }
 else {
 /* Send the distance to the computer using Serial protocol, and
 turn LED OFF to indicate successful reading. */
 Serial.println(distance);


  display.clearDisplay();
    display.setTextColor(WHITE);
    display.setCursor(0,0);
    display.println(distance);
    display.display();
 
 digitalWrite(LEDPin, LOW);
 }
 
 //Delay 50ms before next reading.
 delay(50);





}
